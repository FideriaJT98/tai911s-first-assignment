### A Pluto.jl notebook ###
# v0.19.40

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local iv = try Base.loaded_modules[Base.PkgId(Base.UUID("6e696c72-6542-2067-7265-42206c756150"), "AbstractPlutoDingetjes")].Bonds.initial_value catch; b -> missing; end
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : iv(el)
        el
    end
end

# ╔═╡ 458b35b0-f392-11ee-0342-51b5bba43b4d
# ╠═╡ show_logs = false
begin
    import Pkg
    # activate the shared project environment
    Pkg.activate("C:\\Users\\ndapopilef\\Desktop\\MASTER\\TAM\\Final")
    # instantiate, i.e. make sure that all packages are downloaded
    Pkg.instantiate()

	using MLJLIBSVMInterface
	using CategoricalArrays
	using InteractiveUtils
	using MLJLinearModels
	using Statistics
	using StatsBase
	using RDatasets
	using Markdown
	using Random
	using Printf
	using Plots
	using MLJ
end

# ╔═╡ 7c4a47d8-b3e9-4c59-88d8-da61ecaba75e
using PlutoUI; PlutoUI.TableOfContents()

# ╔═╡ 43c20bc7-b673-423a-8d29-80d8da6225f4
md"""

| Student Name | Student Number |
|-------------|------------|
| Ndapopile Fideria | 217094139 |
| Ngwena Sarlote | 212398233 |
| Kambode Ericka | 213088525 |
| Nuunyango Clemens | 211019410 |

"""

# ╔═╡ 62845d34-29d7-479e-88cc-6f36d8ca886e
md"# TAI911S-First Assignment"

# ╔═╡ fed14be6-ea7d-4a81-bb7e-adba37aaf087
md"## Notes on the Dataset"

# ╔═╡ e957ff32-73b3-4134-b003-0c5f38d40c46
md"""
The dataset is about the **Heating System Choice in California Houses**

The dataset used in this assigment shows that Installation and Operating Costs of 5 systems, and the predicted Costs if the Alternative system where installed under the same conditions.
The conditions are: Income, Rooms and Region

Link to Context of the Dataset
[Click](https://vincentarelbundock.github.io/Rdatasets/doc/Ecdat/Heating.html).
"""

# ╔═╡ 79f3f48a-0b01-47e0-971a-4410b36238e0
md"## Load the dataset"

# ╔═╡ 386c7e4e-481f-4461-9901-2f3612d09c2c
df_heating = RDatasets.dataset("Ecdat", "Heating")

# ╔═╡ a98814e3-2fc2-4e7f-aaed-e1b45aed7b0b
md"## Explore the dataset"
#https://medium.com/@o.eleftherakou/getting-started-with-a-dataset-in-julia-5c69aa4ef6a2

# ╔═╡ 1568fddb-f44b-4c2b-8b33-a582f1ca0cbd
md"### 1. Basic Exploration"

# ╔═╡ f329f31b-402a-4993-b5d8-2627c35917e0
md"""Available Columns"""

# ╔═╡ aead0835-f642-45d9-8191-19f800fb9c2a
names(df_heating)

# ╔═╡ 3e0c8563-ee4a-4840-a6cb-e56c48f0de17
md"""Dimensions of the Dataset"""

# ╔═╡ e176e04c-e787-4e6f-a360-bbbbcf925a0d
rows, columns = size(df_heating)

# ╔═╡ a97a3b92-45d9-4b2e-aa0d-833d6cb63c1a
md"""Preview of the Dataset"""

# ╔═╡ fc5e33c9-70c7-4cfe-9f6a-8615c767d4d0
first(df_heating, 5)

# ╔═╡ 4f1450a0-1364-4913-b4c8-45c40c4aa494
md"""All values in column DepVar"""

# ╔═╡ 2d66e5f7-b78f-47d5-9a5c-1f915cc596c4
levels(df_heating.DepVar)

# ╔═╡ a7019873-ca88-4626-94d0-9b4afa4974c1
md"""All values in column Region"""

# ╔═╡ 46d08c2f-9cb8-43a7-aa5a-f8678e51534f
levels(df_heating.Region)

# ╔═╡ 89e9590b-0dc8-4e07-9c18-48ed95ccfe27
md"""Overall description of the Dataset"""

# ╔═╡ 30376969-3e44-4966-bf50-f15a53146eca
describe(df_heating)

# ╔═╡ 8f937b06-f69e-4b22-8eec-d88364a41f7a
begin
	gp = combine(groupby(df_heating, [:DepVar]), nrow => :count)
	bar(gp.DepVar, gp.count, ylabel="Count", xlabel="Heating System", legend = :none, texts = gp[!, "count"], title = "Choice of Heating Systems")
end

# ╔═╡ 54f7d12e-e146-4026-ba28-c748797f5b6f
md"### 2. Dict Declaration"

# ╔═╡ ca601992-2115-44e8-ac32-40d1964f32ba
begin
	dict_depvar = Dict("gc"=>1, "er"=>2, "gr"=>3, "hp"=>4, "ec"=>5);
	dict_region = Dict("mountn"=>1, "ncostl"=>2, "scostl"=>3, "valley"=>4);
	dict_type = Dict("OC"=>"Operation Cost", "IC"=>"Installation Cost");
	dict_heating = Dict("gas central"=>"gc", "electric room"=>"er", "gas room"=>"gr", "heat pump"=>"hp", "electric central"=>"ec");
	
	dict_depvar_reverse = Dict(values(dict_depvar) .=> keys(dict_depvar))
end

# ╔═╡ ba93ff2e-3a87-4dcb-95ab-d44540d2dd1c
DataFrame(dict_heating)

# ╔═╡ af9996fc-78dd-45f5-897e-cbb427fe4591
DataFrame(dict_depvar)

# ╔═╡ 31898042-765a-46cf-8071-6fa230760c10
DataFrame(dict_region)

# ╔═╡ 0a2202b4-1931-428f-bb3e-d8009b5dedbf
DataFrame(dict_type)

# ╔═╡ 0517efaf-7b7d-4ed0-a964-b5d3cff3428b
md"### 3. Replace Categorical Values"

# ╔═╡ 44436b17-0596-40f8-a166-35d9dd3ade78
md"""Before replacements"""

# ╔═╡ ec9bb133-f2e3-4052-ab29-62764804014c
begin
	df_schema_old = MLJ.schema(copy(df_heating))
	println(rpad(" Name", 28), "| Type")
	println("-"^45)
	for (name, scitype) in zip(df_schema_old.names, df_schema_old.types)
	    println(rpad("$name", 30), scitype)
	end
end

# ╔═╡ f06ed872-5368-4e0a-bac1-32d0e7f9a3bf
md"""As seen above, DepVar and Region are of type CategoricalValue\
We will be changing then to intergers using the Dicts defined earlier"""

# ╔═╡ b32b4877-f771-422f-bdbc-00e4ca517f64
begin
	df_copy = copy(df_heating)
	insertcols!(df_copy, :DepVar_new => -1)
	insertcols!(df_copy, :Region_new => -1)
	for i in 1:size(df_copy)[1]
		df_copy[i,:DepVar_new] = dict_depvar[df_copy[i,:DepVar]]
		df_copy[i,:Region_new] = dict_region[df_copy[i,:Region]]
	end
	select!(df_copy, Not(:DepVar,:Region))
	rename!(df_copy,:DepVar_new => :DepVar)
	rename!(df_copy,:Region_new => :Region)
	
	#Keeping the Columns in the same order 
	df = df_copy[:,["IDCase","DepVar","IC_GC","IC_GR","IC_EC","IC_ER","IC_HP","OC_GC","OC_GR","OC_EC","OC_ER","OC_HP","Income","AgeHed","Rooms","Region","PB_GC","PB_GR","PB_EC","PB_ER","PB_HP"]]
end

# ╔═╡ 9a20e08f-ea9d-4ad3-a740-2b2efa53ca74
md"""After replacements, DepVar and Region are now of type Int64"""

# ╔═╡ 67c00216-c5b6-4533-b967-b098e8a26f85
begin
	df_schema_new = MLJ.schema(df)
	println(rpad(" Name", 28), "| Type")
	println("-"^45)
	for (name, scitype) in zip(df_schema_new.names, df_schema_new.types)
	    println(rpad("$name", 30), scitype)
	end
end

# ╔═╡ fba7e2b2-78f5-4abf-980b-40c256b02748
md"### 4. Correlation of target variable Rooms to other features"

# ╔═╡ af91f85c-a160-4a11-9498-09eecc66479c
begin
	df_cor = cor(Matrix(copy(df)))
	rooms_index = findfirst(isequal("Rooms"),names(df))
	rooms_correlation_with_rooms = df_cor[rooms_index,:]
	rooms_correlation_without_rooms = deleteat!(deepcopy(rooms_correlation_with_rooms),rooms_index)
end

# ╔═╡ 98dabe0e-1dc0-445f-a742-12c4b65289f4
rooms_correlation_without_rooms

# ╔═╡ 18bb8b7f-0328-4877-8a4a-157432344c7d
begin
	cor_str =  [(@sprintf("%.3f",yi),10) for yi in rooms_correlation_without_rooms]
	bar(names(select(copy(df),Not(:Rooms))),rooms_correlation_without_rooms, legend=false, title="Correlation of Rooms against other Features", color=:pink, titlefont = font(13,"Computer Modern"))
	annotate!(names(select(copy(df),Not(:Rooms))),rooms_correlation_without_rooms,cor_str,:bottom)
end

# ╔═╡ 77ecaa18-b611-4516-b498-37bdddacd030
md"""####
The correlation calculation and bar plot provides a visual representation of the relationships between all features of the heating dataset. We can observe that:

- Most features show very weak correlations with rooms, both positive and negative.
- The strongest correlation with rooms is with pb_ec (0.057), indicating a very weal< positive relationship.
- Features such as oc\_gc, ic\_gc also show weak positive correlations.
- Features like agehed and pb_hp show weak negative correlations.

Given these weak correlations, it is clear that rooms may not be strongly predicted by other single features within this dataset. This observation suggests the complexity of predicting rooms directly based on these features alone.
\
\
\
\
"""

# ╔═╡ dc39c633-4aae-4322-9ed4-c73645c884d0
function create_heatmap(df_heating)
	cor_matrix = cor(Matrix(df_heating))
	(n,m) = size(cor_matrix)
	column_names = names(df_heating)
	heatmap(cor_matrix, fc=cgrad([:white,:dodgerblue4]), xticks=(1:m,column_names), xrot=90, yticks=(1:m,column_names), yflip=true)
	#size = (3000, 3000)
	annotate!([(j, i, text(round(cor_matrix[i,j],digits=3), 4,"Computer Modern",:black)) for i in 1:n for j in 1:m])
end

# ╔═╡ 7b795ad5-fa28-4b00-bb76-9fd669edfe25
md"""Correlation of the df as it is"""

# ╔═╡ 3eed10e8-dbbf-42d8-98e4-6af15754a82b
create_heatmap(df)

# ╔═╡ d5ca5b6a-6ba2-4ef8-95d6-b449990f5617
md"""The RDataset plotted above contains RDatase cost predictions of using the alternative Heating Systems\
We will remove the cost predictions below to see if Rooms a high correlations with other features"""

# ╔═╡ 3a66ae62-449a-4f8c-9ac4-4a1f24f4b7c9
function generate_df_without_cost_predictions(act_sys_int)
	act_sys = dict_depvar_reverse[act_sys_int]
	act_sys_oc = "OC_"*uppercase(act_sys)
	act_sys_ic = "IC_"*uppercase(act_sys)
	act_sys_pb = "PB_"*uppercase(act_sys)
	
	ff = df[(df.DepVar .== act_sys_int), :]
	final_df = ff[:,["IDCase",act_sys_oc,act_sys_ic,act_sys_pb,"Income","AgeHed","Rooms","Region"]]
	rename!(final_df,act_sys_oc => "OC")
	rename!(final_df,act_sys_ic => "IC")
	rename!(final_df,act_sys_pb => "PB")
	return final_df
end

# ╔═╡ 3b262c37-8a5e-4585-89de-4bba3a749945
begin
	gc = generate_df_without_cost_predictions(1)
	gr = generate_df_without_cost_predictions(2)
	ec = generate_df_without_cost_predictions(3)
	er = generate_df_without_cost_predictions(4)
	hp = generate_df_without_cost_predictions(5)
	
	df_without_cost_predictions = vcat(gc,gr,ec,er,hp)
end

# ╔═╡ d8859f1f-fa80-4fa6-9b32-3859710e2555
begin
	df_cor_1 = cor(Matrix(copy(df_without_cost_predictions)))
	rooms_index_1 = findfirst(isequal("Rooms"),names(df_without_cost_predictions))
	rooms_correlation_with_rooms_1 = df_cor_1[rooms_index_1,:]
	rooms_correlation_without_rooms_1 = deleteat!(deepcopy(rooms_correlation_with_rooms_1),rooms_index_1)
end

# ╔═╡ bc2129dc-cee4-4954-a8e5-4d92914c234b
begin
	cor_str_1 =  [(@sprintf("%.3f",yi),10) for yi in rooms_correlation_without_rooms_1]
	bar(names(select(copy(df_without_cost_predictions),Not(:Rooms))),rooms_correlation_without_rooms_1, legend=false, title="Correlation of Rooms against other Features - Cost Predictions Removed", color=:yellow, titlefont = font(12,"Computer Modern"))
	annotate!(names(select(copy(df_without_cost_predictions),Not(:Rooms))),rooms_correlation_without_rooms_1,cor_str_1,:bottom)
end

# ╔═╡ c812e9dc-885c-45db-b7c4-886820aa199f
md"""
The highest correlation in the above plot is 0.042 which is lower then the 0.057 from the dataset that has cost predictions

Our attempt to remove the cost predictions in hopes of getting higher correlations did not yield desirable results.
\
\
\
"""

# ╔═╡ 569a8f76-438d-40a9-b4dd-3076ce21d2e4
create_heatmap(df_without_cost_predictions)

# ╔═╡ ef094c37-7449-46b7-a6f4-7e0b7c0b92f1
md"### 5. Rooms based Models"

# ╔═╡ ee3fa80f-092c-4cfa-bbc0-83fed145da50
md"""The above heatmaps and bar plots indicate that Rooms is not a good target, we will none the less create a Models around it.\
This is to indicate that Rooms will produce poor performing models"""

# ╔═╡ 135511b7-3696-4e0b-a3d0-e19bb60618f8
md"""
We will be creating models using the below 2 classification models, this is because Rooms column has a dicrete number of values (2 to 7), that best suit classification.


| Package     | Model      |
|-------------|------------|
| LIBSVM      | SVC        |
| LIBSVM      | NuSVC      |

"""

# ╔═╡ 1daab83d-411b-4097-a790-510150a42c23
md"##### Creating Models from the Dataset with Cost Predictions"

# ╔═╡ e7943d2d-6cce-42f4-b5f6-ed18685252fa
md"""
###### SVC Model Implementation
"""

# ╔═╡ 448cf3bb-753d-4a60-93f2-ac76808564e3
function classification_model_svc(inner_df)
	coerce!(inner_df, :Rooms => OrderedFactor)

	y, X = MLJ.unpack(inner_df, ==(:Rooms), colname -> true, shuffle -> true)
	train_set, test_set = partition(eachindex(y), 0.8)
	@load SVC pkg=LIBSVM
	svc_model = SVC()
	svc = machine(svc_model, X, y, scitype_check_level=0)
	the_predictor = fit!(svc, rows=train_set)

	train_prediction = MLJ.predict(the_predictor, rows=train_set)
	cm = confusion_matrix(train_prediction, y[train_set])
	train_accuracy = accuracy(MLJ.predict(the_predictor, rows=train_set), y[train_set])
	test_accuracy = accuracy(MLJ.predict(the_predictor, rows=test_set), y[test_set])
	return cm, train_accuracy, test_accuracy
end

# ╔═╡ 2dab61db-990d-45bb-b03d-f99aad679776
md"""
###### SVC Model Results
"""

# ╔═╡ 58377f4f-9810-40a7-b1e3-d7d7c1553753
cm_svc, train_accuracy_svc, test_accuracy_svc = classification_model_svc(copy(df))

# ╔═╡ 39b23aee-4e15-41f1-8e20-fbcaf6fee188
md"""The Train accuracy is $(train_accuracy_svc)"""

# ╔═╡ 681e986f-b435-4bd0-9aae-ba12dc9fb838
md"""The Test accuracy is $(test_accuracy_svc)"""

# ╔═╡ 7be74be5-bf05-40bb-a45c-380604a1ecbd
md"""
###### Nu_SVC Model Implementation
"""

# ╔═╡ 7aba4924-e941-46c7-9621-7d514b6a62e8
function classification_model_nusvc(inner_df)
	coerce!(inner_df, :Rooms => OrderedFactor)

	y, X = MLJ.unpack(inner_df, ==(:Rooms), colname -> true, shuffle -> true)
	train_set, test_set = partition(eachindex(y), 0.8)
	@load NuSVC pkg=LIBSVM
	svc_model = NuSVC()
	svc = machine(svc_model, X, y, scitype_check_level=0)
	the_predictor = fit!(svc, rows=train_set)

	train_prediction = MLJ.predict(the_predictor, rows=train_set)
	cm = confusion_matrix(train_prediction, y[train_set])
	train_accuracy = accuracy(MLJ.predict(the_predictor, rows=train_set), y[train_set])
	test_accuracy = accuracy(MLJ.predict(the_predictor, rows=test_set), y[test_set])
	return cm, train_accuracy, test_accuracy
end

# ╔═╡ 60c1bb03-bbce-43b9-b6f5-21afe6bd032f
md"""
###### Nu_SVC Model Results
"""

# ╔═╡ a4e24da9-04f3-415a-83eb-b1ee62dadd6e
cm_nu_svc, train_accuracy_nusvc, test_accuracy_nusvc = classification_model_nusvc(copy(df))

# ╔═╡ cd0d8e5c-ab92-437e-9f43-16cf42ee1c3f
md"##### Creating Models from the Dataset **without** Cost Predictions"

# ╔═╡ 48266fd9-e598-4957-a2dd-62d18af90a4b
md"""
###### SVC Model Results
"""

# ╔═╡ c438d753-4a38-40f6-9072-27b7b997180a
cm_without_cost_predictions_svc, train_accuracy_without_cost_predictions_svc, test_accuracy_without_cost_predictions_svc = classification_model_svc(copy(df_without_cost_predictions))

# ╔═╡ c3788264-6fcd-4b53-8d82-92c70660f912
md"""The Train accuracy is $(train_accuracy_without_cost_predictions_svc)"""

# ╔═╡ 69270965-a222-4ec6-9dcb-488fcb73c2a8
md"""The Test accuracy is $(test_accuracy_without_cost_predictions_svc)"""

# ╔═╡ c6a03a00-9871-4486-b238-c80e7c55c433
md"""
###### Nu_SVC Model Results
"""

# ╔═╡ be611a0a-2a68-4488-92cf-9756b9617a39
cm_without_cost_predictions_nusvc, train_accuracy_without_cost_predictions_nusvc, test_accuracy_without_cost_predictions_nusvc = classification_model_nusvc(copy(df_without_cost_predictions))

# ╔═╡ e7b6a76f-8d85-42b8-82bb-4eb4c456b5fb
md"### 6. Conclusion on Rooms as a Target"

# ╔═╡ d9a396b3-f26b-4c3e-b8ed-2e7ec1670d18
md"""The above Confusion Matrixes, Training and Test Set Acurracy of when the target is Rooms, is proof that Rooms is a poor choice as a target in this particular dataset.

The models from the Dataset with and without the Cost Predicts yield low accuracy rates.


| DF Contains Cost Predictions | Model      |Train Accuracy | Test Accuracy|
|-------------|------------|------------|------------|
| Yes | SVC |21|13|
| Yes | NuSVC |34|11|
| No | SVC |19|16|
| No | NuSVC |23|17|

We will thus continue with a different approach.\
We will be trying to figure out what model was used to predict the Cost of alternative Heating Systems\
"""

# ╔═╡ 895bed26-0818-4c09-9ab0-6a08e3da6624
md"## Extract the features and labels"

# ╔═╡ 631398f5-55ae-4511-b7e0-6e278a40cd5c
md"""The below solution is dynamic.

Select the Main System, and the Alternative System we are going to predict
"""

# ╔═╡ 26f3d762-1792-4802-9fb3-ac0fb92b643b
md"""Select Main System : """

# ╔═╡ 6716cef3-ccd7-46cd-9f26-2af559e2b386
begin
	heating_systems = ["gas central", "gas room","electric central","electric room","heat pump"]
	@bind act_sys_ Select(heating_systems)
end

# ╔═╡ 7ac1a822-c060-4b50-97bc-20c255a0d80b
md"""Select Alternative System : """

# ╔═╡ 8d14c489-b7f8-4461-9803-3f8bbe5ddf2e
@bind pred_sys_ Select(filter!(e->e≠act_sys_,copy(heating_systems)))

# ╔═╡ e95df894-3e94-4f7d-af39-4dacf5012c80
md"""Select Cost : """

# ╔═╡ 66dba55d-40d0-4ac9-a2b8-39bf8b71b22c
@bind cost_type Select(["OC", "IC"])

# ╔═╡ c52908cf-a9dd-4739-b7ff-b7eb650c0ddb
md"""##### Approach

We will not partition a dataframe in 80:20 ratio as traditionally expected, 
Instead we will use all the rows of the Selected Alternative Heating system as the training set, the testing set will be all the rows of the Main System.

**Example:** 
- Select Main System **gas central**
- Select Alternative System **heat pump**
- Select **OC** as the Cost

Gas Central has takes up 573 rows as shows in the _Basic Exploration_ segment\
Heat Pump has takes up 50 rows as shows in the _Basic Exploration_ segment

The Test set will be of 50 rows, and the Training set will be of 573 rows thus.\
\
\
"""

# ╔═╡ 902fa6e0-3c97-44d9-a613-9a2fc3e1d341
function generate_pred_df(act_sys, act_sys_, type_)
	ff = df[(df.DepVar .== act_sys), :]
	final_df = ff[:,["IDCase",act_sys_,"Income","AgeHed","Rooms","Region"]]
	rename!(final_df,act_sys_ => type_)
	return final_df
end

# ╔═╡ 68691793-12f0-4841-8e3d-84cba2dc1f4b
function generate_actual_df(act_sys, act_sys_,alt, type_)
	ff = df[(df.DepVar .== act_sys), :]
	final_df = ff[:,["IDCase",act_sys_,alt,"Income","AgeHed","Rooms","Region"]]
	rename!(final_df,act_sys_ => type_)
	rename!(final_df,alt => "RDataset_Prediction")
	return final_df
end

# ╔═╡ d75eb2a0-ca86-41a5-9490-df8c937901f5
function extract_features(pred_sys_int, type_)
	pred_sys = dict_depvar_reverse[pred_sys_int]
	pred_sys_ = type_*"_"*uppercase(pred_sys)

	pred_df = generate_pred_df(pred_sys_int,pred_sys_,type_)
	coerce!(pred_df, :IDCase => Continuous, :Income  => Continuous, :AgeHed => Continuous, :Rooms => Continuous, :Region => Continuous, type_ => Continuous)
    y, X = MLJ.unpack(pred_df, ==(Symbol(type_)), colname -> true)
	return y, X
end

# ╔═╡ 12c4f49d-0852-4dc4-8bbb-93490f5fad23
begin
	pred_sys = dict_depvar[dict_heating[pred_sys_]]
	y, X = extract_features(pred_sys, cost_type)
end

# ╔═╡ 57b20635-ec26-4355-855e-617d6ed58c8c
begin
	act_sys = dict_depvar[dict_heating[act_sys_]]
	act_sys_1 = cost_type*"_"*uppercase(dict_depvar_reverse[act_sys])
	pred_sys_1 = cost_type*"_"*uppercase(dict_depvar_reverse[pred_sys])
	df_prediction = generate_actual_df(act_sys, act_sys_1,pred_sys_1,cost_type)

	prediction_input = df_prediction[:,["IDCase","Income","AgeHed","Rooms","Region"]]
	prediction_rdataset = df_prediction.RDataset_Prediction
end

# ╔═╡ 432418aa-2e51-4e08-96ea-bcd78d4b4ef6
md"""###### The Training Set Input without Cost Values"""

# ╔═╡ 1c34e7a3-bebb-4bee-840e-2a023a24ef65
prediction_input

# ╔═╡ e2c42ebd-050d-4275-8c8a-61ded6d65741
md"""###### The prediction values from the Rdataset"""

# ╔═╡ 8b75791b-0b06-4616-9db1-475c086216b5
prediction_rdataset

# ╔═╡ f4cb1a5c-b175-49aa-b21b-ff7acc205bd1
md"## Train the model"

# ╔═╡ d5bc5f3d-c88f-43f0-8779-bb211142605a
md"""
We will be training with the regression models listed below\
This is to see which model closes resembles the RDataset cost predictions found on the original dataset


| Package               | Model             |
|-----------------------|-------------------|
| MLJLinearModels       | QuantileRegressor |
| MLJLinearModels       | LinearRegressor   |
| MLJLinearModels       | RidgeRegressor    |
| MLJLinearModels       | LassoRegressor    |

"""

# ╔═╡ 4b2c5094-758a-4c02-b894-c5dac865b727
function train_model(pred_input,y,X)
	reg = @load LassoRegressor pkg = "MLJLinearModels" verbosity=0
	reg_model = reg()
	pred_machine = machine(reg_model, X, y, scitype_check_level=0)
	fit!(pred_machine, rows=collect(eachindex(y)), verbosity=0)
	all_pred_inner = MLJ.predict(pred_machine, pred_input)
	
	return all_pred_inner, pred_machine
end

# ╔═╡ 8304fe68-0caf-4c03-bab8-bf807a47b400
md"""###### Add Predicted Values to dataframe"""

# ╔═╡ 03118bc3-3e39-4b8a-9afa-8aec10ba3dc3
begin
	our_prediction, gg = train_model(prediction_input,y,X)
	df_prediction[!,"Our_Prediction"] = our_prediction
	df_prediction
end

# ╔═╡ c0b88323-c5a7-4957-997b-df7436f7ff02
md"## Evaluate the Model"

# ╔═╡ d18a10a8-7541-4f6d-8f9e-591aee827c9a
md"### RMS: Root mean squared error"

# ╔═╡ 793b0937-76cd-46c4-a8d2-6f53273d3700
md"""![Alt text](https://miro.medium.com/v2/resize:fit:1400/1*ZD1AurFymt1Xd4QPma08xg.gif)"""

# ╔═╡ 6421b0f2-fdac-4c1b-9f3e-636db3f7f3df
md"""##### Approach
We are measuring the Rdataset Predicts against Our prediction\
\
"""

# ╔═╡ 673ffe3b-56fe-45fc-a97d-03f1de2a329b
md"""The rms of when **Main System** is $(act_sys_), **Alternative System** is $(pred_sys_) and the **Cost Type** is $(cost_type)\
$(MLJ.rms(our_prediction, prediction_rdataset))"""

# ╔═╡ c1957116-ada0-4ee4-a7ee-201d0fb4e613
md"""###### How RMS is calculated"""

# ╔═╡ 828356b1-bd01-4f20-81ac-66c2f212de6a
md"""RMS of first 3 points : """

# ╔═╡ e8791bf2-c144-408e-878b-ee20c9ff0230
MLJ.rms(our_prediction[1:3], prediction_rdataset[1:3])

# ╔═╡ a7ff1172-f46a-4c8c-a9f3-954cbbdf64c4
md"""Manual calculation of RMS of the 3 points : """

# ╔═╡ 8d4d838b-f96f-485e-8f91-70b0e7a0e374
begin
	point_1 = MLJ.rms(our_prediction[1], prediction_rdataset[1]).^2
	point_2 = MLJ.rms(our_prediction[2], prediction_rdataset[2]).^2
	point_3 = MLJ.rms(our_prediction[3], prediction_rdataset[3]).^2

	summation = ((point_1 + point_2 + point_3)/3).^0.5
end

# ╔═╡ 15a82a48-58c4-4d56-b5fb-e2d39e9b60da
md"""Ideally we want the RMS of Zero, where theres no difference between 2 corresponding points\
\
If not Zero, the next best model is that which has the rms value closest to Zero"""

# ╔═╡ e19ec367-7066-4dd9-b8f9-d8e5165e6b14
MLJ.rms(our_prediction[1:3], our_prediction[1:3])

# ╔═╡ 318d1996-3227-4de2-b339-2b77d5aac3a4
md"### Plot the Predictions"

# ╔═╡ c50bd9d5-5a95-4ad5-aee9-8cbc183ddb07
function plot_prediction(act_df_, act_sys_int, pred_sys_int, type_, f_size)
	#act_df_ = act_df__[(act_df__.IDCase .< 300), :]#plot a few points only
	pred_sys = dict_depvar_reverse[pred_sys_int]
	act_sys = dict_depvar_reverse[act_sys_int]
	
	the_plot = plot(xlabel="IDCase", ylabel=dict_type[type_], legend=:best ,title = dict_type[type_]*" if "*uppercase(pred_sys)*" was installed instead of "*uppercase(act_sys), titlefont = font(f_size,"Helvetica"))
	
	plot!(act_df_.IDCase, act_df_[:,type_], label=act_sys*" - Actual", seriestype=:scatter, color=:red)
	
	plot!(act_df_.IDCase, act_df_.RDataset_Prediction, label=pred_sys*" - RDataset Prediction", seriestype=:scatter, color=:yellow)
	
	plot!(act_df_.IDCase, act_df_[:,"Our_Prediction"], label=pred_sys*" - Our Prediction", seriestype=:scatter, color=:green)
	
	return the_plot
end

# ╔═╡ 7079c611-4149-43b1-bd91-fe251e09b7d1
p=plot_prediction(copy(df_prediction),act_sys, pred_sys, cost_type, 14)

# ╔═╡ a452f5cc-06d0-429a-9bdd-856f484b10e4
md"""

Below are the 4 model rms values for the below example:\
**Example:** 
- Select Main System **gas central**
- Select Alternative System **heat pump**
- Select **OC** as the Cost

##### Installation Cost 
| Model | Main System Selected | Alternative System Selected | RMS |
|--------|--------|-------|--------|
| QuantileRegressor | gas central | heat pump | 400 |
| LinearRegressor | gas central | heat pump | 162 |
| RidgeRegressor | gas central | heat pump | 161 |
| LassoRegressor | gas central | heat pump | 248 |


##### Operation Cost 
| Model | Main System Selected | Alternative System Selected | RMS |
|--------|--------|-------|--------|
| QuantileRegressor | gas central | heat pump | 70 |
| LinearRegressor | gas central | heat pump | 34 |
| RidgeRegressor | gas central | heat pump | 34 |
| LassoRegressor | gas central | heat pump | 52 |

RidgeRegressor is the best performing model according to the RMS values for both Installation and Operation Cost.\
\
Visually Lasso seems to closey fit the RDataset Predictions.\
The below segment allows us to predict the costs of all alternative systems of a selected Main System.
\
\
"""

# ╔═╡ 0de735d4-8167-480a-92f8-764bf1751463
md"""### Predict All Alternatives of a System"""

# ╔═╡ 97a6518d-eb93-4c9c-b40b-13abb4c078fe
md"""Select Main System"""

# ╔═╡ 5ca73ac1-7e1d-4641-ae0c-4909a5e0a1df
@bind main_system Select(heating_systems)

# ╔═╡ 4a0eeab3-17a3-46d8-8fcb-0458dc3d156a
function all_steps(act_sys_, pred_sys_, cost_type)
	act_sys = dict_depvar[dict_heating[act_sys_]]
	pred_sys = dict_depvar[dict_heating[pred_sys_]]
	#println(act_sys," - ",pred_sys," - ",cost_type)

	y_inner, X_inner = extract_features(pred_sys, cost_type)

	act_sys_1 = cost_type*"_"*uppercase(dict_depvar_reverse[act_sys])
	pred_sys_1 = cost_type*"_"*uppercase(dict_depvar_reverse[pred_sys])
	df_prediction_inner = generate_actual_df(act_sys, act_sys_1,pred_sys_1,cost_type)

	prediction_input_inner = df_prediction_inner[:,["IDCase","Income","AgeHed","Rooms","Region"]]
	prediction_rdataset_inner = df_prediction_inner.RDataset_Prediction
	
	all_pred_inner, hh = train_model(prediction_input_inner,y_inner,X_inner)
	df_prediction_inner[!,"Our_Prediction"] = all_pred_inner
	final_plot = plot_prediction(df_prediction_inner,act_sys, pred_sys, cost_type, 8)
		
	return final_plot, MLJ.rms(all_pred_inner, prediction_rdataset_inner)
end

# ╔═╡ a53d0a38-f2e7-46a1-9f34-30186391f3c7
md"""##### 1. Installation Cost"""

# ╔═╡ 5f2d73e3-59df-4d23-a67b-283ccf456000
begin
	alternatives = copy(heating_systems)
	filter!(e->e≠main_system,alternatives)
	p_1, rms_1 = all_steps(main_system, alternatives[1], "IC")
	p_2, rms_2 = all_steps(main_system, alternatives[2], "IC")
	p_3, rms_3 = all_steps(main_system, alternatives[3], "IC")
	p_4, rms_4 = all_steps(main_system, alternatives[4], "IC")
	
	plot(p_1, p_2, p_3, p_4,layout = (2, 2), size = (700, 600), cbar = false)
end

# ╔═╡ e32888a2-2a42-492a-a092-b05c9ee59f2e
md"""
The **rms** is $rms_1\
The **rms** is $rms_2\
The **rms** is $rms_3\
The **rms** is $rms_4\
"""

# ╔═╡ 6ab687ee-9513-4990-9195-0588b7710154
md"""##### 2. Operating Cost"""

# ╔═╡ 36f55706-07ad-4c28-8d14-37cec26dc069
begin
	p_1_, rms_1_ = all_steps(main_system, alternatives[1], "OC")
	p_2_, rms_2_ = all_steps(main_system, alternatives[2], "OC")
	p_3_, rms_3_ = all_steps(main_system, alternatives[3], "OC")
	p_4_, rms_4_ = all_steps(main_system, alternatives[4], "OC")
	
	plot(p_1_, p_2_, p_3_, p_4_,
         layout = (2, 2), size = (700, 600), cbar = false)
end

# ╔═╡ c089d133-b519-4d0b-85d1-46e0d3d53a60
md"""
The **rms** is $rms_1_\
The **rms** is $rms_2_\
The **rms** is $rms_3_\
The **rms** is $rms_4_\
"""

# ╔═╡ Cell order:
# ╠═7c4a47d8-b3e9-4c59-88d8-da61ecaba75e
# ╠═458b35b0-f392-11ee-0342-51b5bba43b4d
# ╟─43c20bc7-b673-423a-8d29-80d8da6225f4
# ╠═62845d34-29d7-479e-88cc-6f36d8ca886e
# ╟─fed14be6-ea7d-4a81-bb7e-adba37aaf087
# ╟─e957ff32-73b3-4134-b003-0c5f38d40c46
# ╟─79f3f48a-0b01-47e0-971a-4410b36238e0
# ╠═386c7e4e-481f-4461-9901-2f3612d09c2c
# ╟─a98814e3-2fc2-4e7f-aaed-e1b45aed7b0b
# ╟─1568fddb-f44b-4c2b-8b33-a582f1ca0cbd
# ╟─f329f31b-402a-4993-b5d8-2627c35917e0
# ╠═aead0835-f642-45d9-8191-19f800fb9c2a
# ╟─3e0c8563-ee4a-4840-a6cb-e56c48f0de17
# ╠═e176e04c-e787-4e6f-a360-bbbbcf925a0d
# ╟─a97a3b92-45d9-4b2e-aa0d-833d6cb63c1a
# ╠═fc5e33c9-70c7-4cfe-9f6a-8615c767d4d0
# ╟─4f1450a0-1364-4913-b4c8-45c40c4aa494
# ╠═2d66e5f7-b78f-47d5-9a5c-1f915cc596c4
# ╟─a7019873-ca88-4626-94d0-9b4afa4974c1
# ╠═46d08c2f-9cb8-43a7-aa5a-f8678e51534f
# ╟─89e9590b-0dc8-4e07-9c18-48ed95ccfe27
# ╠═30376969-3e44-4966-bf50-f15a53146eca
# ╠═8f937b06-f69e-4b22-8eec-d88364a41f7a
# ╟─54f7d12e-e146-4026-ba28-c748797f5b6f
# ╠═ca601992-2115-44e8-ac32-40d1964f32ba
# ╠═ba93ff2e-3a87-4dcb-95ab-d44540d2dd1c
# ╠═af9996fc-78dd-45f5-897e-cbb427fe4591
# ╠═31898042-765a-46cf-8071-6fa230760c10
# ╠═0a2202b4-1931-428f-bb3e-d8009b5dedbf
# ╟─0517efaf-7b7d-4ed0-a964-b5d3cff3428b
# ╟─44436b17-0596-40f8-a166-35d9dd3ade78
# ╠═ec9bb133-f2e3-4052-ab29-62764804014c
# ╟─f06ed872-5368-4e0a-bac1-32d0e7f9a3bf
# ╠═b32b4877-f771-422f-bdbc-00e4ca517f64
# ╟─9a20e08f-ea9d-4ad3-a740-2b2efa53ca74
# ╠═67c00216-c5b6-4533-b967-b098e8a26f85
# ╟─fba7e2b2-78f5-4abf-980b-40c256b02748
# ╠═af91f85c-a160-4a11-9498-09eecc66479c
# ╠═98dabe0e-1dc0-445f-a742-12c4b65289f4
# ╠═18bb8b7f-0328-4877-8a4a-157432344c7d
# ╟─77ecaa18-b611-4516-b498-37bdddacd030
# ╠═dc39c633-4aae-4322-9ed4-c73645c884d0
# ╟─7b795ad5-fa28-4b00-bb76-9fd669edfe25
# ╠═3eed10e8-dbbf-42d8-98e4-6af15754a82b
# ╟─d5ca5b6a-6ba2-4ef8-95d6-b449990f5617
# ╠═3a66ae62-449a-4f8c-9ac4-4a1f24f4b7c9
# ╠═3b262c37-8a5e-4585-89de-4bba3a749945
# ╠═d8859f1f-fa80-4fa6-9b32-3859710e2555
# ╠═bc2129dc-cee4-4954-a8e5-4d92914c234b
# ╟─c812e9dc-885c-45db-b7c4-886820aa199f
# ╠═569a8f76-438d-40a9-b4dd-3076ce21d2e4
# ╟─ef094c37-7449-46b7-a6f4-7e0b7c0b92f1
# ╟─ee3fa80f-092c-4cfa-bbc0-83fed145da50
# ╟─135511b7-3696-4e0b-a3d0-e19bb60618f8
# ╟─1daab83d-411b-4097-a790-510150a42c23
# ╟─e7943d2d-6cce-42f4-b5f6-ed18685252fa
# ╠═448cf3bb-753d-4a60-93f2-ac76808564e3
# ╟─2dab61db-990d-45bb-b03d-f99aad679776
# ╠═58377f4f-9810-40a7-b1e3-d7d7c1553753
# ╟─39b23aee-4e15-41f1-8e20-fbcaf6fee188
# ╟─681e986f-b435-4bd0-9aae-ba12dc9fb838
# ╟─7be74be5-bf05-40bb-a45c-380604a1ecbd
# ╠═7aba4924-e941-46c7-9621-7d514b6a62e8
# ╟─60c1bb03-bbce-43b9-b6f5-21afe6bd032f
# ╠═a4e24da9-04f3-415a-83eb-b1ee62dadd6e
# ╟─cd0d8e5c-ab92-437e-9f43-16cf42ee1c3f
# ╟─48266fd9-e598-4957-a2dd-62d18af90a4b
# ╠═c438d753-4a38-40f6-9072-27b7b997180a
# ╟─c3788264-6fcd-4b53-8d82-92c70660f912
# ╟─69270965-a222-4ec6-9dcb-488fcb73c2a8
# ╟─c6a03a00-9871-4486-b238-c80e7c55c433
# ╠═be611a0a-2a68-4488-92cf-9756b9617a39
# ╟─e7b6a76f-8d85-42b8-82bb-4eb4c456b5fb
# ╟─d9a396b3-f26b-4c3e-b8ed-2e7ec1670d18
# ╟─895bed26-0818-4c09-9ab0-6a08e3da6624
# ╟─631398f5-55ae-4511-b7e0-6e278a40cd5c
# ╟─26f3d762-1792-4802-9fb3-ac0fb92b643b
# ╠═6716cef3-ccd7-46cd-9f26-2af559e2b386
# ╟─7ac1a822-c060-4b50-97bc-20c255a0d80b
# ╠═8d14c489-b7f8-4461-9803-3f8bbe5ddf2e
# ╟─e95df894-3e94-4f7d-af39-4dacf5012c80
# ╠═66dba55d-40d0-4ac9-a2b8-39bf8b71b22c
# ╟─c52908cf-a9dd-4739-b7ff-b7eb650c0ddb
# ╠═902fa6e0-3c97-44d9-a613-9a2fc3e1d341
# ╠═68691793-12f0-4841-8e3d-84cba2dc1f4b
# ╠═d75eb2a0-ca86-41a5-9490-df8c937901f5
# ╠═12c4f49d-0852-4dc4-8bbb-93490f5fad23
# ╠═57b20635-ec26-4355-855e-617d6ed58c8c
# ╟─432418aa-2e51-4e08-96ea-bcd78d4b4ef6
# ╠═1c34e7a3-bebb-4bee-840e-2a023a24ef65
# ╟─e2c42ebd-050d-4275-8c8a-61ded6d65741
# ╠═8b75791b-0b06-4616-9db1-475c086216b5
# ╟─f4cb1a5c-b175-49aa-b21b-ff7acc205bd1
# ╟─d5bc5f3d-c88f-43f0-8779-bb211142605a
# ╠═4b2c5094-758a-4c02-b894-c5dac865b727
# ╟─8304fe68-0caf-4c03-bab8-bf807a47b400
# ╠═03118bc3-3e39-4b8a-9afa-8aec10ba3dc3
# ╟─c0b88323-c5a7-4957-997b-df7436f7ff02
# ╟─d18a10a8-7541-4f6d-8f9e-591aee827c9a
# ╟─793b0937-76cd-46c4-a8d2-6f53273d3700
# ╟─6421b0f2-fdac-4c1b-9f3e-636db3f7f3df
# ╟─673ffe3b-56fe-45fc-a97d-03f1de2a329b
# ╟─c1957116-ada0-4ee4-a7ee-201d0fb4e613
# ╟─828356b1-bd01-4f20-81ac-66c2f212de6a
# ╠═e8791bf2-c144-408e-878b-ee20c9ff0230
# ╟─a7ff1172-f46a-4c8c-a9f3-954cbbdf64c4
# ╠═8d4d838b-f96f-485e-8f91-70b0e7a0e374
# ╟─15a82a48-58c4-4d56-b5fb-e2d39e9b60da
# ╠═e19ec367-7066-4dd9-b8f9-d8e5165e6b14
# ╟─318d1996-3227-4de2-b339-2b77d5aac3a4
# ╠═c50bd9d5-5a95-4ad5-aee9-8cbc183ddb07
# ╠═7079c611-4149-43b1-bd91-fe251e09b7d1
# ╟─a452f5cc-06d0-429a-9bdd-856f484b10e4
# ╟─0de735d4-8167-480a-92f8-764bf1751463
# ╟─97a6518d-eb93-4c9c-b40b-13abb4c078fe
# ╠═5ca73ac1-7e1d-4641-ae0c-4909a5e0a1df
# ╠═4a0eeab3-17a3-46d8-8fcb-0458dc3d156a
# ╟─a53d0a38-f2e7-46a1-9f34-30186391f3c7
# ╟─5f2d73e3-59df-4d23-a67b-283ccf456000
# ╠═e32888a2-2a42-492a-a092-b05c9ee59f2e
# ╟─6ab687ee-9513-4990-9195-0588b7710154
# ╟─36f55706-07ad-4c28-8d14-37cec26dc069
# ╠═c089d133-b519-4d0b-85d1-46e0d3d53a60
